package cn.tiakon.dmp.etl

import cn.tiakon.dmp.untils.{ContextUtils, Utils}
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.{DataFrame, SQLContext}

/**
  * 使用 sql 的方式将原始日志( .logs 文件)转换成 parquet 文件
  *
  * @author Tiakon
  *         2018/3/28 19:49
  */
object Sql2Parquet {
  def main(args: Array[String]): Unit = {

    val load: Config = ConfigFactory.load()

    val sc = ContextUtils.getSparkContext()

    val sqlContext: SQLContext = new SQLContext(sc)

    Utils.deleteFileByCoreSite(sc, load.getString("output.parquet.path"))

    val dataFrame: DataFrame = sqlContext.read.text(load.getString("input.parquet.path"))

    dataFrame.write.parquet(load.getString("output.parquet.path"))
    //释放资源
    sc.stop()
  }
}
