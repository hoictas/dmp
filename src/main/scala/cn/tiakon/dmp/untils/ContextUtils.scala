package cn.tiakon.dmp.untils

import org.apache.spark.{SparkConf, SparkContext}


/**
  * @author Tiakon
  *         2018/3/28 13:28
  */
object ContextUtils {
  /**
    * 获得 SparkContext 对象
    */
  def getSparkContext(): SparkContext = {
    val conf = new SparkConf()
      .setMaster("local[*]")
      .setAppName(this.getClass.getName)
      .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
      .set("spark.sql.parquet.compression.codec", "snappy")
    new SparkContext(conf)
  }
}
