package cn.tiakon.dmp.untils

/**
 * 包装一个新的 String 对象
 *
 * @author Tiakon
 * 2018/3/28 22:59
 */
class StringPlus(val str: String) {

    def toIntPlus: Int = {
        try {
            str.toInt
        } catch {
            case _: Exception => 0
        }
    }

    def toDoublePlus: Double = {
        try {
            str.toDouble
        } catch {
            case _: Exception => 0
        }
    }
}


object StringPlus{
    implicit def str2StringPlus(str: String): StringPlus = new StringPlus(str)
}
